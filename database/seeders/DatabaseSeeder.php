<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $this->call([
            CategorySeeder::class,
            UserSeeder::class,
            PostSeeder::class,
            CommentSeeder::class,
        ]);

        // $this->call(class: UserSeeder::class);
        // $this->call(class: CategorySeeder::class);
        // $this->call(class: PostSeeder::class);
        // $this->call(class: CommentSeeder::class);
    }
}
