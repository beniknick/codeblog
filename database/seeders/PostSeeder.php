<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use App\Models\Post;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Add random categories to a post
        $categories = Category::all(); //Create a variable with all the categories from the Category model
        //Create 20 post 
        Post::factory(20)->create()->each(function ($post) use ($categories) {
            $post->categories()->attach( //For each/loop $post use $categories and the function to attach 
                $categories->random(rand(1, 3))->pluck('id')->toArray()
                //For each post pluck category id en store in a array to attach 1-3 random categories to a post
            );
        });
    }
}
