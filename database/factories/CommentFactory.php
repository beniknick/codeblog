<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;


class CommentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'post_id' => rand(1, 20),
            'user_id' => rand(1, 20),
            'content' => $this->faker->text,
            'visible' => $this->faker->boolean,
        ];
    }
}
