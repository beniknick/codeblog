<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;



class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => rand(1, 20),
            'title' => $this->faker->word,
            'content' => $this->faker->text,
            'image' => $this->faker->imageUrl($width = 640, $height = 480),
            'visible' => $this->faker->boolean,
        ];
    }
}
