@extends('/components/master')

@section('title', 'Homepage')

@section('content')


<div class="createCommentBlock">
   <h1>Comment on the post</h1>

   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif

   <form method="POST" action="{{ route('comment.store') }}">
      <input type="text" name="title" placeholder="Title...">
      <br><br>
      <textarea name="content" rows="5" cols="70" placeholder="Your comment"></textarea>
      <br><br>
      <input type="hidden" id="1" name="visible" value="false">
      <input type="checkbox" id="1" name="visible" value="true">
      <label for="1">Private</label>
      <br><br>
      <input type="hidden" name="post_id" value="{{$post->id}}" />
      {{csrf_field()}}
      <button class="button" type="submit">Post your comment</button>
      <a class="button" href=" {{ url()->previous() }}">Go Back</a>
   </form>
</div>

@endsection