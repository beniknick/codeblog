@extends('/components/master')

@section('title', 'Homepage')

@section('content')


<div class="formBlock">
   <h1>Edit your post</h1>

   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif

   <form method="PUT" action="{{route('comment.update', $comment->id)}}">

      <input type="text" name="title"><?= $comment->title ?></input>
      <br>
      <textarea name="content" rows="5" cols="70"><?= $comment->content ?></textarea>
      <br><br>
      <input type="hidden" id="1" name="visible" value="false">
      <input type="checkbox" id="1" name="visible" value="true">
      <label for="1">Private</label>
      {{csrf_field()}}
      <button class="button" type="submit">Save your comment</button>
      <a class="button" href=" {{ url()->previous() }}">Go Back</a>
   </form>
</div>

@endsection