<!DOCTYPE html>
<html lang="en">

<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <title>@yield('title')</title>
   <link rel="stylesheet" type="text/css" href="{{ asset('master.css') }}">
   <!-- <link rel="stylesheet" type="text/css" href="/public/master.css"> -->

</head>

<body>
   <header class="header_block pat5">
      <h1>CODE BLOG</h1>
   </header>

   <nav class="navbar">
      <a href="/">Home</a> <!-- use the /user to start the uri after the 8000 not after /user  -->
      <a href="{{ route('register')}}">Profile</a>
      <a href="/post/create">Add Post</a>
      <div class="dropdown">
         <button class="dropbtn">Categories</button>
         <div class="dropdown-content">
            @foreach ($categories as $category)
            <a href="{{ route('category.post.index', ['category' => $category->id])}}">{{$category->title}}</a>
            @endforeach
         </div>
      </div>
      </div>
      <a href=" /about">About</a>
      <span class="float_right">
         @guest
         <a href="{{ route('login')}}">Login</a>
         @endguest
         @auth
         <a href="/premium">Go premium</a>
         <a href="{{ route('logout')}}">Logout</a>
         @endauth
      </span>

   </nav>

   <div class="content_block">

      @yield('content')
   </div>

   <footer class="footer_block">
      <p>Footer</p>
   </footer>
</body>

</html>