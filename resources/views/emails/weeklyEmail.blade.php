@component('mail::message')
#Weekly update

This are the posts you have missed last week.
<br>
<div class="list">
   @foreach($post as $post)
   <div class="post-container">
      <div class="post-block item1">
         &#128100; {{ $post->user->user_name}}
         <!-- &#129485; -->
      </div>
      <div class="post-block item2">
         @foreach ($post->categories as $category)
         <!--  $category as one loop of the total 
         categories table linked too the $posts object/table -->
         <a class="button category-btn" href="{{ route('category.post.index', ['category' => $category->id])}}">{{$category->title}}</a>
         @endforeach
      </div>
      <div class="post-block item3">
         <h3> {{$post->title}}</h3>
         <p> {{$post->content}}</p>
         <br>
         <div class="btn-set">
            <a class="button" href="{{route('post.show', $post->id)}}">view post</a>
            <a class="button" href="{{route('comment.create', $post->id)}}">create comment</a>
         </div>
      </div>
      <div class="post-block item4">
         <div class="pic-temp">
            @if($post->image)
            <img class="pic-temp" src="{{asset($post->image)}}" alt="your image">
            @endif
            @if ($post->image == Null)
            <img class="pic-temp" src="{{url('images/not-found-image.jpg')}}" alt=" not found image">
            @endif
         </div>
         <div class="add-at">
            created at: {{$post->user->created_at}}
         </div>
      </div>
   </div>
   @endforeach
</div>
<br>
<br>
@component('mail::button', ['url' => ''])
show the posts
@endcomponent

Thanks,<br>
Nobody

@endcomponent