@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="loginBlock">
    <h2>Reset your password</h2>
    <br>
    <!-- Session Status -->
    <auth-session-status class="mb-4" status="session('status')">

        <!-- Validation Errors -->
        <auth-validation-errors class="mb-4" errors="$errors">

            <form method="POST" action="{{ route('password.email') }}">
                @csrf

                <!-- Email Address -->
                <div>
                    <label for="email" value="__('Email')">

                        <input id="email" class="block mt-1 w-full" type="email" name="email" value="old('email')" required autofocus>
                </div>
                <br>
                <div class="flex items-center justify-end mt-4">
                    <button class="button">
                        {{ __('Reset Password') }}
                    </button>
                </div>
            </form>

</div>

@endsection