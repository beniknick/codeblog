@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="loginBlock">
    <h2>Login for your next story</h2>
    <br>
    <!-- Session Status -->
    <!-- <auth-session-status class="mb-4" :status="session('status')" /> -->

    <!-- Validation Errors -->
    <!-- <auth-validation-errors class="mb-4" :errors="$errors" /> -->

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <!-- Email Address -->
        <div>
            <label for="email" value="__('Email')">

                <input id="email" type="email" name="email" value="Your email" required autofocus>
        </div>
        <!-- Password -->
        <div class="mt-4">
            <label for="password" value="__('Password')">

                <input id="password" type="password" name="password" required autocomplete="current-password">
        </div>

        <!-- Remember Me -->
        <div class="block mt-4">
            <label for="remember_me">
                <input id="remember_me" type="checkbox" name="remember">
                <span>{{ __('Remember me') }}</span>
            </label>
        </div>
        <br>
        <div>
            @if (Route::has('password.request'))
            <a href="{{ route('password.request') }}">
                {{ __('Forgot your password?') }}
            </a>
            @endif
            <br>
            <br>
            <button class="button">
                {{ __('Log in') }}
            </button>
            <br>
            <a class="button" href="{{ route('register')}}">Make now a Account</a>
        </div>
    </form>

</div>

@endsection