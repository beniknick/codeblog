@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="registrationBlock">
    <h2>Register today for your premium account</h2>
    <br>
    <!-- Validation Errors -->
    <auth-validation-errors errors="$errors">

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- User_name -->
            <div>
                <label for="user_name" value="__('user_name')">
                    <input id="user_name" type="text" name="user_name" placeholder="User name" required autofocus>
            </div>
            <!-- First_name -->
            <div>
                <label for="first_name" value="__('first_name')">
                    <input id="first_name" type="text" name="first_name" placeholder="First name" required autofocus>
            </div>
            <!-- Last_name -->
            <div>
                <label for="last_name" value="__('last_name')">
                    <input id="last_name" type="text" name="last_name" placeholder="Last name" required autofocus>
            </div>

            <!-- Email Address -->
            <div>
                <label for="email" value="__('email')">
                    <input id="email" type="email" name="email" placeholder="Email" required>
            </div>

            <!-- Password -->
            <div>
                <label for="password" value="__('password')">
                    <input id="password" type="password" name="password" placeholder="Password" required autocomplete="new-password">
            </div>

            <!-- Confirm Password -->
            <div>
                <label for="password_confirmation" value="__('confirm password')">
                    <input id="password_confirmation" type="password" name="password_confirmation" placeholder="Confirm password" required>
            </div>
            <!-- Premium Abbo -->
            <div>
                <label for="1">Premium Abbo</label>
                <input type="hidden" id="1" name="abbo_active" value="0">
                <input type="checkbox" id="1" name="abbo_active" value="1">
            </div>

            <div>
                <a href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>
                <br>
                <br>
                <button class="button">
                    {{ __('Register') }}
                </button>
            </div>
        </form>

</div>

@endsection