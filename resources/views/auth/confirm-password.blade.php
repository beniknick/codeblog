@extends('/components/master')

@section('title', 'Homepage')

@section('content')


<div class="loginBlock">
    <h2>Confirm your password now</h2>
    <br>
    <!-- Validation Errors -->
    <auth-validation-errors errors="$errors">

        <form method="POST" action="{{ route('password.confirm') }}">
            @csrf

            <!-- Password -->
            <div>
                <label for="password" value="__('Password')">

                    <input id="password" type="password" name="password" required autocomplete="current-password">
            </div>

            <div ">
                <button class=" button">
                {{ __('Confirm') }}
                </button>
            </div>
        </form>

</div>

@endsection