@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="formBlock">
   <h1>Create a new category</h1>

   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif

   <form method="POST" action="{{ route('category.store') }}">
      <input type="text" name="title" placeholder="Title...">
      <br><br>
      {{csrf_field()}}
      <button type="submit">Create your category</button>
   </form>
</div>

@endsection