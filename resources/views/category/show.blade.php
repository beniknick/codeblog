@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<h1>show categories page</h1>

<div class="list">
   @foreach($post as $post)
   <!-- Receives a $posts object from the UserController  -->
   <!--  $post as one loop of the total $posts -->

   <div class="post-container">
      <div class="post-block item1">
         &#128100; {{ $post->user->user_name}}
         <!-- &#129485; -->
      </div>
      <div class="post-block item2">
         @foreach ($post->categories as $category)
         <!--  $category as one loop of the total 
         categories table linked too the $posts object/table -->
         <a class="button category-btn" href="/">{{$category->title}}</a>
         @endforeach
      </div>
      <div class="post-block item3">
         <h3> {{$post->title}}</h3>
         <p> {{$post->content}}</p>
         <br>
         <div class="btn-set">
            <a class="button" href="{{route('post.show', $post->id)}}">view post</a>
            <a class="button" href="{{route('comment.create', $post->id)}}">Comment</a>
         </div>
      </div>
      <div class="post-block item4">
         <div class="pic-temp">
            <img class="pic-temp" src="{{url('/images/not-found-image.jpg')}}" alt="image not found">
         </div>
         <div class="add-at">
            created at: {{$post->user->created_at}}
         </div>
      </div>

   </div>

   @endforeach

</div>

@endsection