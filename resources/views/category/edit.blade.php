@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="formBlock">
   <h1>Edit a category</h1>

   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif

   <form method="PUT" action="{{route('category.update', $category->id)}}">
      <input type="text" name="title" value="<?= $category->title ?>">
      <br><br>
      {{csrf_field()}}
      <button type="submit">Edit your category</button>
   </form>
</div>

@endsection