@extends('/components/master')

@section('title', 'Homepage')

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
   <ul>
      @foreach ($errors->all() as $error)
      <li>{{ $error }}</li>
      @endforeach
   </ul>
</div>
@endif

<div class="formBlock">
   <h1>Create a new post</h1>

   <form method="POST" action="{{ route('post.store') }}" enctype="multipart/form-data">
      @csrf
      <input type="text" name="title" placeholder="Title...">
      <br>
      <select type="text" name="category_id[]" multiple>
         <!-- multiple -->
         @foreach ($categories as $category)
         <option value="{{$category->id}}">{{$category->title}}</option>
         @endforeach
      </select>
      <a href="{{route('category.create')}}">Add category</a>
      <br><br>
      <textarea name="content" rows="5" cols="70">Your story of the day...</textarea>
      <br>
      <div class="">
         Add a image:
         <input class="" type="file" name="image">
      </div>
      <br>
      <input type="hidden" id="1" name="visible" value="0">
      <input type="checkbox" id="1" name="visible" value="1">
      <label for="1">Private</label>
      <br><br>

      <button class="button" type="submit">Post it</button>
   </form>
</div>

@endsection