@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="post-container">
   <div class="post-block item1">
      &#128100; {{ $post->user->user_name}}
      <!-- &#129485; -->
   </div>
   <div class="post-block item2">
      @foreach ($post->categories as $category)
      <!--  $category as one loop of the total 
         categories table linked too the $posts object/table -->
      <a class="button category-btn" href="/">{{$category->title}}</a>
      @endforeach
   </div>
   <div class="post-block item3">
      <h3> {{$post->title}}</h3>
      <p> {{$post->content}}</p>
      <br>
      <div class="btn-set">
         <form method="POST" action="{{route('post.destroy', $post->id)}}">
            @method('DELETE')
            <!-- to set the methode to delete -->
            @csrf
            <a class="button" href="{{route('comment.create', $post->id)}}">Comment</a>
            @if($post->user_id == Auth::id())
            <a class="button" href="{{route('post.edit', $post->id)}}">Edit</a>
            <input class="button" type="submit" value="destroy" name="destroy">
            @endif
         </form>
      </div>
   </div>
   <div class="post-block item4">
      <div class="pic-temp">
         <div class="pic-temp">
            @if($post->image)
            <img class="pic-temp" src="{{asset($post->image)}}" alt="your image">
            @endif
            @if ($post->image == Null)
            <img class="pic-temp" src="{{url('images/not-found-image.jpg')}}" alt=" not found image">
            @endif
         </div>
      </div>
      <div class="add-at">
         created at: {{$post->user->created_at}}
      </div>
   </div>

</div>


<?php //dd($post);
?>

@foreach ($post->comments as $comment)

<div class="commentBlock">
   <h4>&#128100; {{$comment->user->user_name}} </h4>
   <p>{{$comment->content}}</p>
   <br>
   @if($comment->user_id == Auth::id())
   <a class="button" href="{{route('comment.edit', $comment->id)}}">Edit</a>
   <a class="button" href="{{route('comment.destroy', $comment->id)}}">Remove</a>
   @endif
   <div class="float-right">
      created at: {{$comment->created_at}}
   </div>
</div>

@endforeach






@endsection