<?php

use Symfony\Component\Console\Input\Input;
?>
@extends('/components/master')

@section('title', 'Homepage')

@section('content')

<div class="formBlock">
   <h1>Edit your post</h1>

   @if ($errors->any())
   <div class="alert alert-danger">
      <ul>
         @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
         @endforeach
      </ul>
   </div>
   @endif

   <form method="PUT" action="{{route('post.update', $post->id)}}">
      @csrf
      <input type="text" name="title" value=<?= $post->title ?>>
      <br>
      <select type="text" name="category_id">
         <!-- multiple -->
         @foreach ($post->categories as $category)
         <option value="{{$category->id}}">{{$category->title}}</option>
         @endforeach
      </select>
      <br><br>
      <textarea name="content" rows="7" cols="70" value=<?= $post->content ?>><?= $post->content ?></textarea>
      <br>
      <div class="">
         Add a image:
         <input class="" type="file" name="image">
      </div>
      <br>
      <input type="hidden" id="1" name="visible" value="0">
      <input type="checkbox" id="1" name="visible" value="1">
      <label for="1">Private</label>
      <br><br>
      <button class="button" type="submit">Save</button>
      <a class="button" href=" {{ url()->previous() }}">Go Back</a>
   </form>
</div>


<?php
// <select type="text" name="category_id">
// @foreach ($categories as $category)
// <option value="{{$category->id}}">{{$category-> category}}</option>
// @endforeach
// </select>
?>



@endsection