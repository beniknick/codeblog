@extends('/components/master')

@section('title', 'Homepage')

@section('content')


<div class="aboutBlock">

   <h1>Go premium now!</h1>



   <form method="POST" action="{{route('user.premium')}}">
      @csrf
      @method("PUT")

      <!-- Premium Abbo -->
      <div>

         <input type="checkbox" id="1" name="abbo_active" value="1">
         <label for="1">Premium Abbo on</label>
         <br>
         <input type="checkbox" id="1" name="abbo_active" value="0">
         <label for="1">Premium Abbo off</label>
      </div>
      <br>
      <div>
         <button class="button">
            {{ __('Pay or Change') }}
         </button>
         <br>
         Now just 10,- a month
      </div>
   </form>

</div>

@endsection