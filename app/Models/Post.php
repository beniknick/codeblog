<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $guarded = ['user_id', 'created_at', 'updated_at'];


    public function user()
    {   //Posts table belongs to one cell in the users table through the User model
        return $this->belongsTo(User::class);
    }

    public function categories()
    {   //Posts table belongs to many cells in the categories table through the Category model
        return $this->belongsToMany(Category::class);
    }

    public function comments()
    {   //Posts table has many relations with the comments table through the Comment model
        return $this->hasMany(Comment::class);
    }
}
