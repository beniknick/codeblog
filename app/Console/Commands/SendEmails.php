<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\WeeklyEmail;
use App\Models\Post;
use Illuminate\Support\Facades\Mail;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:weeklyEmail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends the weekly update email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $posts = Post::latest()->take(3)->get();
        Mail::to('beniknick@hotmail.com')->send(new WeeklyEmail($posts));
        echo 'The emails are send';
    }
}
