<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Http\Requests\CommentRequest;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $comment = Comment::with("comment")->get();
        // $comment = comment::find(1)->Comment;
        // return view('comment.index');  //, ['comment' => $comment]
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create(Post $post)
    {
        if (Auth::user()) {
            return view('comment.create', ['post' => $post]);
        } else {
            return redirect()->route('login');
        };
        return view('comment.create', ['post' => $post]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\CommentRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        $comment = new Comment();
        $comment->post_id = $request->post_id;   //foreignId $request->post_id
        $comment->user_id = Auth::id();    //foreignId
        $comment->content = $request->content;
        $comment->visible = $request->visible;  //Boolean
        $comment->save();  //save the request by de hand of de model

        return redirect()->route('post.show', ['post' => $request->post_id]);
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        $comment->load('user');
        return view('comment.edit', compact('comment'));
    }



    /**
     * Update the specified resource in storage.
     * @param  \App\Http\Requests\CommentRequest  $request
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        $comment->post_id = $request->get('post_id');   //foreignId
        $comment->user_id =  $request->get('user_id');  //foreignId
        $comment->content = $request->get('content');
        $comment->visible = $request->get('visible');   //Boolean
        $comment->save();

        return redirect()->route('post.show', ['post' => $request->post_id]);
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        $post_id = $comment->post_id;   //Set $post_id value in a variable to use it after de delete function
        $comment->delete();
        return redirect()->route('post.show', ['post' => $post_id]);
        //Go back to the post you delete from with a route model binding
    }
}
