<?php

namespace App\Http\Controllers;


use App\Models\Post;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with('user', 'categories')->OrderByDesc('created_at')->where('visible', 0)->get();

        if (Auth::user() && Auth::user()->abbo_active) {   //it will check for booleans values is true if not what else
            //Eager loading. Get all the posts and the relations in the user and categories table 
            $posts = Post::with('user', 'categories')->OrderByDesc('created_at')->get();
        }
        //Sends a $post object to use in the user.index view file 
        return view('index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()) {
            return view('post.create');
        } else {
            return redirect()->route('login');
        };
    }

    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\PostRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        // dd($request);
        //Create a new post in a row off the Post model/database
        $post = new Post();   //Store single cells in new row
        $post->user_id = Auth::id();   //Get id of the current user
        $post->title = $request->title;
        $post->content = $request->content;

        if ($path = $request->file('image')) {
            $path = $request->file('image')->store('images');
            $post->image =  $path;
        }

        $post->visible = $request->visible;  //Boolean
        $post->save();  //Save the request by de hand of de model

        $post->categories()->attach($request->category_id);  //


        // if ($request->hasFile('image')) {

        // }

        // $category = new category();  //Store one to one or one to many
        // $category->email = $email;
        // $post->category()->save($categories);

        // $categoryPost = new CategoryPost();  //Store many to many
        // $categoryPost->categoryPost()->attach([  //attach or sync To insert the data into pivot table you can use the attach method on the relationship
        //    $post->id,
        //    $categories->id
        // ]);

        return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)  //Route model Binding. Gets all the post model/table as a object in the $post value
    {
        //Route model Binding. To find a specific post that also loads the table relations
        $post->load('user', 'categories', 'comments');
        return view('post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $post->load('user', 'categories');
        return view('post.edit', compact('post'));  // , ['categories' => Category::all()]
    }

    /**
     * Update the specified resource in storage.
     * @param  \App\Http\Requests\PostRequest  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, Post $post)
    {
        $post->fill($request->validated());  //A object from the request validation to fill the value of $post
        $post->save();
        return redirect()->route('post.show', $post->id);

        //     $post->user_id =  $request->get('user_id');  //Getting values from the request of the database grocery
        //     $post->title = $request->get('title');
        //     $post->content = $request->get('content');
        //     $post->visible = $request->get('visible');
        //     $post->save();
        //     return redirect()->route('post.show', $post->id);
        //     //return redirect()->back(); //->with('status','Student Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();  //add ->onDelete('cascade') to all the foreignId
        return redirect()->route('index');
    }
}
