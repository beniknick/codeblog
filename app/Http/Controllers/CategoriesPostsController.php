<?php

namespace App\Http\Controllers;

use App\Models\Category;


class CategoriesPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Category $category)
    {
        $posts = $category->post;   //Retrieve data through the model/methode name
        //Start with the child of the parent, to find all the posts attached to that parent and send it to the view
        return view('category.post.index', compact('posts'));
    }

    /*
    You use a nested controller when you need a specific item of the parent controller 
    and most of the time you use just the index to show the items. 
    With naming the nested controller you start with the parent that de child in pascal-case and plural/meervoud
    */
}
