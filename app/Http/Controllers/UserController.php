<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Sends a $user object of all users to use in the user.create view file 
        return view('user.create', ['user' => User::all()]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        // $validated = $request->validated();

        // $User = new User();
        // $User->user_name = $validated['user_name']; //Getting values from the request object
        // $User->first_name = $validated['first_name'];
        // $User->last_name = $validated['last_name'];
        // $User->email = $validated['email'];
        // $User->password = $validated['password'];
        // $User->abbo_active = $validated['abbo_active'];
        // $User->save();  //save the request first though the User Request validation to the model

        // return redirect()->route('index');
    }

    /**
     * Display the specified resource.
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)  //Route model Binding. Gets the post model/table as a object in the $post value
    {
        //Route model Binding. To find a specific user that also loads the table relations

        // $user->load('post', 'comments');
        return view('user.show', compact('$user'));
        // return view('user.show', ['user' => User::find(1)]);
    }


    /**
     * Show the form for editing the specified resource.
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function edit(User $User)
    {
        //Sends a $user object of all users to use in the user.create view file 
        return redirect()->route('user.edit', ['user' => $User, 'user' => User::all()]);
    }

    /**
     * Update the specified resource in storage.
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\Models\User  $pUser
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $User)
    {
        $User->fill($request->validated());  //A object from the request validation to fill the value of $post
        $User->save();
        return redirect()->route('index');

        //Getting values though the Update User Request validation of the user model/users table
        // $User->user_name =  $request->get('user_name');
        // $User->first_name = $request->get('first_name');
        // $User->last_name = $request->get('last_name');
        // $User->email = $request->get('email');
        // $User->password = $request->get('password');
        // $User->country = $request->get('country');
        // $User->language = $request->get('language');
        // $User->abbo_active = $request->get('abbo_active');
        // $User->save(); //Save the request first though the User Request validation to the model

        // $User->fill($request->validated());  //A object from the request validation to fill the value of $post
        // $User->save();

        // return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     * @param  \App\Models\User  $User
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $User)
    {
        $User->delete();
        return redirect()->route('index');
    }

    public function updatePremium(Request $request)
    {
        $user = User::find(Auth::id());   //Get id of the current user
        $user->abbo_active = $request->abbo_active;
        $user->save();
        $user->refresh();
        // Auth::setUser($user);
        return redirect()->route('index');
    }
}
