<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => '',
            'title' => 'required|string',
            // 'category_id' => 'string|required|min:2|max:55',
            //'image' => '',  //image|mimes:jpeg,png,jpg,gif,svg|max:2048
            'content' => 'required|string',
            'visible' => '',
        ];
    }
}
