<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_name' => 'required|string|min:2|max:25',
            'first_name' => 'required|string|min:2|max:25',
            'last_name' => 'required|string|min:2|max:25',
            'email' => 'required|email|unique|min:2|max:25',
            'password' => 'required|confirmed|string|min:5|max:25',
            'password_confirmation' => 'required|same:password|min:5',
            'abbo_active' => 'nullable',
        ];
    }
}
