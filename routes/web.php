<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CategoriesPostsController;


//LOCALHOST DIRECT TO HOME PAGE
Route::get('/', [PostController::class, 'index'])->name('index');  //Go to the userController index methode


//RESOURCE ROUTES
Route::resource('user', UserController::class); //Makes route names like user + actions = user.index for all the resources
Route::resource('post', PostController::class);
Route::resource('comment', CommentController::class)->except('create', 'destroy');
Route::get('comment/{post}/create', [CommentController::class, 'create'])->name('comment.create');
Route::get('comment/{comment}/destroy', [CommentController::class, 'destroy'])->name('comment.destroy');
Route::resource('category', CategoryController::class);
Route::resource('category.post', CategoriesPostsController::class);


//OTHER ROUTES
Route::get('/about', function () {  //To redirect to a view with a route name
    return view('/about');
});
Route::get('/login', function () {
    return view('/login');
});
Route::get('/premium', function () {
    return view('/premium');
});
Route::put('update/premium-status', [UserController::class, 'updatePremium'])->name('user.premium');;

//AUTHENTICATION ROUTES
Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/